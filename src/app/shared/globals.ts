export const YOUR_USER = 'user';
export const YOUR_ACCES_TOKEN = 'access_token';
export const YOUR_HEADER_NAME = 'Token';
export const YOUR_USER_NAME = 'user_name';
export const YOUR_PERMISIONS = 'permisions';
export const YOUR_THRESHOLD = 'turnoverThreshold';

export const PERMISSIONS = {};